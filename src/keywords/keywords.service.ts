import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { KeywordDocument } from 'src/schemas/keyword.schema';
import { CreateKeywordDto } from './dto/create-keyword.dto';
import { UpdateKeywordDto } from './dto/update-keyword.dto';
import { Keyword } from './entities/keyword.entity';

@Injectable()
export class KeywordsService {
  constructor(@InjectModel(Keyword.name) private keywordModel : Model<KeywordDocument>  ) {}

  async create(createKeywordDto: CreateKeywordDto):Promise<Keyword> {
    const newKeyword = new this.keywordModel(createKeywordDto);
    return newKeyword.save()
  }
  
  async uploadFile(name : string, columns : any[], originalname : string):Promise<Keyword> {
    const newKeyword =  new this.keywordModel;
    newKeyword.name = name;
    newKeyword.columns = columns
    newKeyword.originalname = originalname
    return newKeyword._id
  }

  async findAll():Promise<Keyword[]> {
    return this.keywordModel.find();
  }

  async findIds():Promise<Keyword[]> {
    return this.keywordModel.find().select("_id");
  }


  async findOne(_id: string): Promise<Keyword> {
    return this.keywordModel.findOne({ _id });
  }

  async update(_id: string, updateKeywordDto: CreateKeywordDto): Promise<Keyword> {
    return this.keywordModel.findByIdAndUpdate({_id},updateKeywordDto)
 }

 remove(_id: string) {
  return this.keywordModel.findOneAndRemove({_id});
}
}
