import { Controller, Get, Post, Body, Patch, Param, Delete, UploadedFile, UseInterceptors, UseGuards } from '@nestjs/common';
import { KeywordsService } from './keywords.service';
import { CreateKeywordDto } from './dto/create-keyword.dto';
import { UpdateKeywordDto } from './dto/update-keyword.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import * as csv from 'csvtojson';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';

@Controller('keywords')
export class KeywordsController {
  constructor(private readonly keywordsService: KeywordsService) {}

  @Post()
  create(@Body() body,@Body() createKeywordDto: any) {
    console.log(body)
    return this.keywordsService.create(createKeywordDto.name = body.name);
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Body() body,
    @Body() createKeywordDto: any,
  ) {
   return csv({
      output: 'line',
      ignoreEmpty: true,
      noheader : true,
      delimiter: "auto",
    })
      .fromString(file.buffer.toString())
      .then((res) => {
        console.log(res)
        // for(var i in res){
        //   console.log(Object.values(res[i]))
        // }
        //console.log(Object.values(res[0]))
        createKeywordDto.columns = res;
        createKeywordDto.name = body.name
        createKeywordDto.originalname = file.originalname;
        createKeywordDto.mimetype = file.mimetype
        createKeywordDto.size = file.size / 1000 //to KB
        return this.keywordsService.create(createKeywordDto)
      });
  }

  @UseGuards(AccessTokenGuard)
  @Get()
  findAll() {
    return this.keywordsService.findAll();
  }

  @Get("all/ids")
  findIds() {
    return this.keywordsService.findIds();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.keywordsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateKeywordDto: UpdateKeywordDto) {
    return this.keywordsService.update(id, updateKeywordDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.keywordsService.remove(id);
  }
}
