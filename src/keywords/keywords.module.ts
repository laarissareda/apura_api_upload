import { Module } from '@nestjs/common';
import { KeywordsService } from './keywords.service';
import { KeywordsController } from './keywords.controller';
import { Keyword, KeywordSchema } from 'src/schemas/keyword.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: Keyword.name, schema: KeywordSchema }])],
  controllers: [KeywordsController],
  providers: [KeywordsService],
  exports:[KeywordsService]
})
export class KeywordsModule {}
