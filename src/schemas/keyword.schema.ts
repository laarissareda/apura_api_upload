import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type KeywordDocument = Keyword & Document

@Schema({
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
})

export class Keyword {
  @Prop({ type: String, required: true})
  name : string

  @Prop({type : String, required : false})
  originalname : string

  @Prop({type : String, required : false})
  mimetype : string

  @Prop({type : String, required : false})
  size : string

  @Prop({type : Array, required : false})
  columns : object []

  @Prop({type : Boolean, default : false})
  favorite : boolean
}

export const KeywordSchema = SchemaFactory.createForClass(Keyword);

