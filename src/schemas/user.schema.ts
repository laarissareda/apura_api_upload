import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document

@Schema({
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
})

export class User {
  @Prop({ type: String, required: true})
  firstname : string

  @Prop({type : String, required : true})
  lastname : string

  @Prop({type : String, required : true, unique: true})
  username : string

  @Prop({type : String, required : true, unique: true})
  email : string

  @Prop({type : String, required : true})
  password : string

  @Prop({type : String, required : false})
  phone_number : string

  @Prop({type : String, required : false})
  hash_token : string

  @Prop({type : String, required : false})
  company : string

  @Prop({type : String, required : false})
  title : string

  @Prop({type : String, required : false})
  about : string
  
}

export const UserSchema = SchemaFactory.createForClass(User);