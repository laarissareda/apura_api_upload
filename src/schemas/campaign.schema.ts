import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CampaignDocument = Campaign & Document

@Schema({
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
})

export class Campaign {
  @Prop({ type: String, required: true})
  name : string

  @Prop({type : String, required : false})
  originalname : string

  @Prop({type : String, required : false})
  mimetype : string

  @Prop({type : String, required : false})
  size : string

  @Prop({type : Array, required : false})
  columns : object []

  
  @Prop({type : Array, required : false})
  config : object []

  @Prop({type : String, required : false, default: 'unknown'})
  quality : string

  @Prop({type : Array, required : false})
  headers : string []
}

export const CampaignSchema = SchemaFactory.createForClass(Campaign);

