import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Query,
  Req,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CampaignsService } from './campaigns.service';
import { UpdateCampaignDto } from './dto/update-campaign.dto';
import * as csv from 'csvtojson';
import { Request } from 'express';

@Controller('campaigns')
export class CampaignsController {
  constructor(private readonly campaignsService: CampaignsService) {}

  // @Post()
  // create(@Body() createCampaignDto: CreateCampaignDto) {
  //   return this.campaignsService.create(createCampaignDto);
  // }

  @Get()
  findAll(@Query() { limit }) {
    return this.campaignsService.findAll(limit);
  }
  
  @Get('search')
  search(@Req() req: Request) {
    return this.campaignsService.search(req);
  }

  @Get('fiche/:id')
  findOne(@Param('id') id: string) {
    return this.campaignsService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCampaignDto: UpdateCampaignDto,
  ) {
    return this.campaignsService.update(id, updateCampaignDto);
     
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.campaignsService.remove(id);
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Body() body,
    @Body() createCampaignDto: any,
  ) {
   return  csv({
      output: 'json',
      ignoreEmpty: true,
      delimiter: ',',
    })
      .fromString(file.buffer.toString())
      .then((res) => {
        createCampaignDto.columns = res;
        createCampaignDto.name = body.name
        createCampaignDto.originalname = file.originalname;
        createCampaignDto.mimetype = file.mimetype
        createCampaignDto.size = file.size / 1000 //to KB
        createCampaignDto.config = {};
        createCampaignDto.headers = Object.keys(res[0])
        return this.campaignsService.create(createCampaignDto)
      });
  }
}
