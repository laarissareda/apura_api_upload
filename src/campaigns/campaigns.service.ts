import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Request } from 'express';
import { Model } from 'mongoose';
import { Campaign, CampaignDocument } from 'src/schemas/campaign.schema';
import { CreateCampaignDto } from './dto/create-campaign.dto';
import { UpdateCampaignDto } from './dto/update-campaign.dto';

@Injectable()
export class CampaignsService {
  constructor(
    @InjectModel(Campaign.name) private campaignModel: Model<CampaignDocument>,
  ) {}

  async create(createCampaignDto: CreateCampaignDto): Promise<Campaign> {
    const newCampaign = new this.campaignModel(createCampaignDto);
    return newCampaign.save();
  }

  async uploadFile(
    name: string,
    columns: any[],
    originalname: string,
  ): Promise<Campaign> {
    const newCampaign = new this.campaignModel();
    newCampaign.name = name;
    newCampaign.columns = columns;
    newCampaign.originalname = originalname;
    return newCampaign._id;
  }

  async findAll(limit?: number): Promise<Campaign[]> {
    return this.campaignModel.find().limit(limit);
  }
  
  async search(request: Request) {
    let options = {} as any;
    if (request.query) {
      options = {
        $or: [
          { name: request.query.name.toString() },
          { originalname: request.query.original.toString() },
          { size: request.query.size.toString() },
        ],
      };
    }
    console.log(options);
    options.deleted = false;
    const query = await this.campaignModel.find(options);
    return query;
  }

  async findOne(_id: string): Promise<Campaign> {
    return this.campaignModel.findOne({ _id });
  }

  async update(
    _id: string,
    updateCampaignDto: UpdateCampaignDto,
  ): Promise<Campaign> {
    return this.campaignModel.findByIdAndUpdate({ _id }, updateCampaignDto);
  }

  remove(_id: string) {
    return this.campaignModel.findOneAndRemove({ _id });
  }
}
