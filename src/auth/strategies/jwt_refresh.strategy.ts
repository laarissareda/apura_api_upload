import { ForbiddenException, Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Request } from "express";
import { ConfigService } from "@nestjs/config";


@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(Strategy, 'jwt-refresh'){
    constructor(config : ConfigService){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey : config.get<String>('RT_SECRET'),
            passReqToCallback : true
        })
    }
    validate (req : Request,payload : any){
        const refreshToken = req
        ?.get('authorization')
        ?.replace('Bearer', '')
        .trim();
  
      if (!refreshToken) throw new ForbiddenException('Refresh token not found');
  
      return {
        ...payload,
        refreshToken,
      };
    }
    
}