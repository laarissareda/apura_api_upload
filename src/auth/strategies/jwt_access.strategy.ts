import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";


type JwtPayload = {
    sub : string,
    email : string
}

@Injectable()
export class JwtAccessStrategy extends PassportStrategy(Strategy, 'jwt'){
    constructor(config : ConfigService){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey : config.get<String>('AT_SECRET')
        })
    }

    validate (payload : JwtPayload){
        return payload;
    }

}