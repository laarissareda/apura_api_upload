import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { Tokens } from './types/tokens.type';
import * as bcrypt from 'bcrypt'

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}

  //* ************* My Utility functions ***************** */
  async getTokens(userId: string, email: string) {
    const [accessToken, refreshToken] = await Promise.all([
      // accessToken
      this.jwtService.signAsync(
        {
          sub: userId,
          email,
        },
        {
          secret: 'at-secret',
          expiresIn: 60 * 1,
        },
      ),
      // refreshToken
      this.jwtService.signAsync(
        {
          sub: userId,
          email,
        },
        {
          secret: 'rt-secret',
          expiresIn: 60 * 60 * 24 * 3, //Refresh token 3 days exp
        },
      ),
    ]);
    return {
      // return as type Token (already created this type)
      access_token: accessToken,
      refresh_token: refreshToken,
    };
  }

  async updateRefreshTokenHash(userId: string, refreshToken: string) {
    const hash = await this.userService.hashPassword(refreshToken);
    const user = await this.userService.findOne(userId);
    //user not found
    if (!user) {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }
    user.hash_token = hash;
    this.userService.update(userId, user)
  }
  //* ************* My Utility functions ***************** */

  async signup(CreateUserDto: CreateUserDto): Promise<Tokens> {
    const newUser = await this.userService.create(CreateUserDto);
      const tokens = await this.getTokens(newUser._id, newUser.email);
      console.log(tokens);
      console.log(newUser);
      await this.updateRefreshTokenHash(newUser._id, tokens.refresh_token);
      return tokens;
  }

  async signin(CreateUserDto: CreateUserDto){
    const user = await this.userService.validateUser(CreateUserDto)

    //user not found
    if (!user) throw new HttpException('user not found', HttpStatus.NOT_FOUND);

    const tokens = await this.getTokens(user._id, user.email)
    await this.updateRefreshTokenHash(user._id , tokens.refresh_token)
    // return tokens

    const connected = {
      currentUser : user,
      tokens : tokens
    }
    return connected;
  }

  async logout(userId: string){
    const user = await this.userService.findOne(userId)
    user.hash_token = null
    this.userService.update(userId, user)
    // then remove the stored jwt from front storage
  }

  async refresh(userId : string, refreshT : string){
    const user = await this.userService.findOne(userId)

    if(!user) throw new HttpException('user not found', HttpStatus.NOT_FOUND)
    const matches = await bcrypt.compare(refreshT, user.hash_token)
    if(!matches) throw new HttpException('token not existing', HttpStatus.FORBIDDEN);

    const tokens = await this.getTokens(userId, user.email)
    await this.updateRefreshTokenHash(userId , tokens.refresh_token)
    return tokens
  }


  async findAll() {
    return this.userService.findAll();
  }

  async findOne(id: string) {
    return this.userService.findOne(id);
  }

  async update(id: string, updateAuthDto: UpdateAuthDto) {
    return this.userService.update(id,updateAuthDto)
  }

  remove(id: string) {
    return this.userService.remove(id)
  }
}
