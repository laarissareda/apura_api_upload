import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from 'src/users/users.module';
import { JwtAccessStrategy } from './strategies/jwt_access.strategy';
import { JwtRefreshStrategy } from './strategies/jwt_refresh.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [UsersModule, JwtModule.register({}), ConfigModule],
  controllers: [AuthController],
  providers: [AuthService, JwtAccessStrategy,JwtRefreshStrategy]
})
export class AuthModule {}
