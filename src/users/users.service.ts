import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { User, UserDocument } from 'src/schemas/user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  //* ************* My Utility functions ***************** */

  // Function to hash the Password
  hashPassword(pass: string) {
    return bcrypt.hash(pass, 10);
  }

  // Function to exclude the pass from User Obj
  sanitizeUser(user: UserDocument) {
    const sanitized = user.toObject();
    delete sanitized['password'];
    delete sanitized['hash_token'];
    return sanitized;
  }

  //* ************* My Utility functions ***************** */

  // function that creates a user and check if already exists
  async create(createUserDto: CreateUserDto) {
    const user = await this.userModel.findOne({
      $or: [
        { email: createUserDto.email },
        { username: createUserDto.username },
      ],
    });
    if (user) {
      throw new HttpException('user already exists', HttpStatus.BAD_REQUEST);
    }
    const newUser = new this.userModel(createUserDto);
    newUser.password = await this.hashPassword(createUserDto.password);

    newUser.save();
    return this.sanitizeUser(newUser);
  }

  // validate user crendantials
  async validateUser(createUserDto: CreateUserDto) {
    const user = await this.userModel.findOne({
      email: createUserDto.email,
    });
    //user not found
    if (!user) throw new ForbiddenException('User not found');

    const passwordMatches = await bcrypt.compare(
      createUserDto.password,
      user.password,
    );
    //password incorrect
    if (!passwordMatches) throw new ForbiddenException('Incorrect Password');

    return this.sanitizeUser(user);
  }

  // Find All Users
  async findAll(): Promise<User[]> {
    return this.userModel.find();
  }

  // Find One user by id
  async findOne(_id: string): Promise<User> {
    return this.userModel.findOne({ _id });
  }

  // Find One user by email
  async findByEmail(email: string): Promise<User> {
    return this.userModel.findOne({ email });
  }

  async update(_id: string, updateUserDto: UpdateUserDto): Promise<User> {
    return this.userModel.findByIdAndUpdate({ _id }, updateUserDto);
  }

  remove(_id: string) {
    return this.userModel.findOneAndRemove({ _id });
  }
}
