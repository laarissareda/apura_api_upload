import { Exclude } from 'class-transformer';
import { IsEmail, IsHash, IsNotEmpty, IsOptional } from 'class-validator';

export class CreateUserDto {
  @IsOptional()
  firstname: string;

  @IsOptional()
  lastname: string;

  @IsNotEmpty()
  username: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  password: string;

  @IsOptional()
  phone_number: string;

  @IsOptional()
  hash_token: string;

  @IsOptional()
  company: string;
  @IsOptional()
  title: string;
}
